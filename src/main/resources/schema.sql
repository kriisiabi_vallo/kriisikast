DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `company`;

CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(190) NOT NULL,
    `password` VARCHAR(190) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
);

CREATE TABLE `company` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `logo` VARCHAR(255) NULL,
  `established` DATE NULL,
  `employees` INT NULL,
  `revenue` DECIMAL(12,2) NULL,
  `net_income` DECIMAL(12,2) NULL,
  `securities` INT NULL,
  `security_price` DECIMAL(10,2) NULL,
  `dividends` DECIMAL(10,2) NULL,
  PRIMARY KEY (id)
);