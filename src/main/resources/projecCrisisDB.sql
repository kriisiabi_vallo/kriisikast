-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for project_crisis
CREATE DATABASE IF NOT EXISTS `project_crisis` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `project_crisis`;

-- Dumping structure for table project_crisis.company
CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `established` date DEFAULT NULL,
  `employees` int(11) DEFAULT NULL,
  `revenue` decimal(12,2) DEFAULT NULL,
  `net_income` decimal(12,2) DEFAULT NULL,
  `securities` int(11) DEFAULT NULL,
  `security_price` decimal(10,2) DEFAULT NULL,
  `dividends` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.company: ~12 rows (approximately)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` (`id`, `name`, `logo`, `established`, `employees`, `revenue`, `net_income`, `securities`, `security_price`, `dividends`) VALUES
	(1, 'Arco Vara', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=ARC', '1994-07-04', 20, 3640000.00, -540000.00, 8998367, 1.09, 0.01),
	(2, 'Baltika', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=BLT', '1997-05-09', 946, 44690000.00, -5120000.00, 4079485, 0.32, NULL),
	(3, 'Ekspress Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=EEG', '1995-06-21', 1698, 60490000.00, 10000.00, 29796841, 0.84, NULL),
	(4, 'Harju Elekter', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=HAE', '1996-05-14', 744, 120800000.00, 1550000.00, 17739880, 4.25, 0.18),
	(5, 'LHV Group', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=LHV', '2005-01-25', 366, 64540000.00, 25240000.00, 26016485, 11.75, 0.21),
	(6, 'Merko Ehitus', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=MRK', '1990-11-05', 740, 418010000.00, 19340000.00, 17700000, 9.30, 1.00),
	(7, 'Nordecon', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=NCN', '1998-01-01', 662, 223500000.00, 3380000.00, 32375483, 1.04, 0.12),
	(8, 'Pro Kapital Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=PKG', '1994-01-01', 91, 27990000.00, 16830000.00, 56687954, 1.43, NULL),
	(9, 'Tallink Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TAL', '1997-08-21', 7201, 949720000.00, 40050000.00, 669882040, 0.96, 0.12),
	(10, 'Tallinna Kaubamaja Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TKM', '1960-07-21', 4293, 681180000.00, 30440000.00, 40729200, 8.36, 0.71),
	(11, 'Tallinna Sadam', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TSM', '1991-12-25', 468, 130640000.00, 24420000.00, 263000000, 1.96, 0.13),
	(12, 'Tallinna Vesi', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TVE', '1967-01-01', 314, 62780000.00, 24150000.00, 20000000, 10.80, 0.75);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;

-- Dumping structure for table project_crisis.condition
CREATE TABLE IF NOT EXISTS `condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condition` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.condition: ~9 rows (approximately)
/*!40000 ALTER TABLE `condition` DISABLE KEYS */;
INSERT INTO `condition` (`id`, `condition`) VALUES
	(2, 'taimetoitlane'),
	(3, 'vegan'),
	(4, 'gluteenitalumatus'),
	(5, 'laktoositalumatus'),
	(6, 'pähkliallergia'),
	(8, 'kass'),
	(9, 'koer'),
	(10, 'omnivoor'),
	(11, 'allergik');
/*!40000 ALTER TABLE `condition` ENABLE KEYS */;

-- Dumping structure for table project_crisis.order
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_method` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=475 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table project_crisis.order: ~42 rows (approximately)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`id`, `date`, `email`, `delivery_method`, `name`, `delivery_address`, `quantity`) VALUES
	(2, '2019-11-14', 'kalle.karp@gmail.com', '1', 'Kalle Karp', '', 2),
	(20, '2019-11-27', 'kiira.naitli@gmail.com', '2', 'Kiira Naitli', 'London UK', 1),
	(24, '2019-11-27', 'tom.kruus@gmail.com', '1', 'Tom Kruus', '', 2),
	(39, '2019-11-28', 'orlando.ploom@gmail.com', '1', 'Orlando Ploom', '', 3),
	(429, '2019-12-04', 'otto@etv.ee', '1', 'Otto Triin', '', 0),
	(463, '2019-12-04', 'pere@kassiga.ee', '2', 'Pere Kassiga', 'Tartu', 0),
	(464, '2019-12-04', 'teet@kuldvillak.ee', '1', 'Teet Margna', '', 0),
	(465, '2019-12-04', 'kalju@pank.com', '1', 'hjgjh', '', 0),
	(466, '2019-12-04', 'a@a.com', '1', 'asd', '', 0),
	(467, '2019-12-06', 'kalju@pank.com', '1', 'erte', '', 0),
	(472, '2019-12-06', 'kalju@pank.com', '1', 'asdf', '', 0),
	(473, '2019-12-06', 'kalju@pank.com', '1', 'asdf', '', 0),
	(474, '2019-12-06', 'kalju@pank.com', '1', 'fghj', '', 0);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Dumping structure for table project_crisis.order_content
CREATE TABLE IF NOT EXISTS `order_content` (
  `order_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  KEY `FK__person` (`person_id`),
  KEY `FK__product` (`product_id`),
  KEY `FK_order_content_order` (`order_id`),
  CONSTRAINT `FK__person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK__product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_order_content_order` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.order_content: ~421 rows (approximately)
/*!40000 ALTER TABLE `order_content` DISABLE KEYS */;
INSERT INTO `order_content` (`order_id`, `person_id`, `product_id`, `quantity`) VALUES
	(2, 5, 1, 2),
	(2, 5, 2, 2),
	(2, 5, 8, 2),
	(2, 5, 12, 3),
	(2, 5, 13, 2),
	(2, 5, 20, 1),
	(2, 5, 23, 1),
	(2, 5, 27, 1),
	(2, 6, 1, 2),
	(2, 6, 2, 2),
	(2, 6, 8, 2),
	(2, 6, 10, 1),
	(2, 6, 12, 1),
	(2, 6, 13, 1),
	(2, 6, 21, 1),
	(2, 6, 22, 1),
	(2, 6, 24, 1),
	(2, 6, 27, 2),
	(2, 7, 26, 6),
	(20, 62, 1, 3),
	(20, 62, 9, 3),
	(20, 62, 12, 1),
	(20, 62, 16, 2),
	(20, 62, 21, 1),
	(20, 62, 22, 1),
	(20, 62, 23, 1),
	(20, 62, 27, 1),
	(20, 63, 1, 2),
	(20, 63, 2, 2),
	(20, 63, 8, 2),
	(20, 63, 12, 3),
	(20, 63, 13, 2),
	(20, 63, 20, 1),
	(20, 63, 23, 1),
	(20, 63, 27, 1),
	(20, 64, 1, 2),
	(20, 64, 2, 2),
	(20, 64, 8, 2),
	(20, 64, 10, 1),
	(20, 64, 12, 1),
	(20, 64, 13, 1),
	(20, 64, 18, 1),
	(20, 64, 19, 1),
	(20, 64, 21, 1),
	(20, 64, 22, 1),
	(20, 64, 23, 1),
	(20, 64, 24, 1),
	(20, 64, 27, 1),
	(24, 74, 1, 3),
	(24, 74, 9, 3),
	(24, 74, 12, 1),
	(24, 74, 16, 2),
	(24, 74, 21, 1),
	(24, 74, 22, 1),
	(24, 74, 23, 1),
	(24, 74, 27, 1),
	(24, 75, 1, 2),
	(24, 75, 2, 2),
	(24, 75, 8, 2),
	(24, 75, 12, 3),
	(24, 75, 13, 2),
	(24, 75, 20, 1),
	(24, 75, 23, 1),
	(24, 75, 27, 1),
	(24, 76, 1, 2),
	(24, 76, 2, 2),
	(24, 76, 8, 2),
	(24, 76, 10, 1),
	(24, 76, 12, 1),
	(24, 76, 13, 1),
	(24, 76, 18, 1),
	(24, 76, 19, 1),
	(24, 76, 21, 1),
	(24, 76, 22, 1),
	(24, 76, 23, 1),
	(24, 76, 24, 1),
	(24, 76, 27, 1),
	(39, 94, 1, 3),
	(39, 94, 9, 3),
	(39, 94, 12, 1),
	(39, 94, 16, 2),
	(39, 94, 21, 1),
	(39, 94, 22, 1),
	(39, 94, 23, 1),
	(39, 94, 27, 1),
	(39, 95, 1, 2),
	(39, 95, 2, 2),
	(39, 95, 8, 2),
	(39, 95, 12, 3),
	(39, 95, 13, 2),
	(39, 95, 20, 1),
	(39, 95, 23, 1),
	(39, 95, 27, 1),
	(39, 96, 1, 2),
	(39, 96, 2, 2),
	(39, 96, 8, 2),
	(39, 96, 10, 1),
	(39, 96, 12, 1),
	(39, 96, 13, 1),
	(39, 96, 18, 1),
	(39, 96, 19, 1),
	(39, 96, 21, 1),
	(39, 96, 22, 1),
	(39, 96, 23, 1),
	(39, 96, 24, 1),
	(39, 96, 27, 1),
	(429, 799, 1, 2),
	(429, 799, 2, 2),
	(429, 799, 8, 2),
	(429, 799, 10, 1),
	(429, 799, 12, 1),
	(429, 799, 13, 1),
	(429, 799, 19, 1),
	(429, 799, 20, 1),
	(429, 799, 21, 1),
	(429, 799, 22, 1),
	(429, 799, 24, 1),
	(429, 799, 27, 1),
	(429, 799, 28, 1),
	(429, 799, 31, 1),
	(429, 799, 32, 2),
	(429, 799, 33, 1),
	(429, 800, 1, 2),
	(429, 800, 2, 2),
	(429, 800, 8, 2),
	(429, 800, 12, 3),
	(429, 800, 20, 1),
	(429, 800, 23, 1),
	(429, 800, 30, 27),
	(463, 841, 1, 1),
	(463, 841, 2, 1),
	(463, 841, 8, 1),
	(463, 841, 10, 2),
	(463, 841, 12, 2),
	(463, 841, 13, 3),
	(463, 841, 19, 1),
	(463, 841, 20, 1),
	(463, 841, 21, 2),
	(463, 841, 22, 2),
	(463, 841, 24, 3),
	(463, 841, 27, 1),
	(463, 841, 28, 3),
	(463, 841, 31, 1),
	(463, 841, 32, 1),
	(463, 841, 33, 2),
	(463, 842, 1, 1),
	(463, 842, 2, 1),
	(463, 842, 8, 1),
	(463, 842, 12, 9),
	(463, 842, 20, 1),
	(463, 842, 23, 1),
	(463, 842, 30, 2),
	(463, 843, 26, 1),
	(464, 844, 1, 1),
	(464, 844, 2, 1),
	(464, 844, 8, 1),
	(464, 844, 10, 3),
	(464, 844, 12, 3),
	(464, 844, 13, 4),
	(464, 844, 18, 1),
	(464, 844, 19, 1),
	(464, 844, 20, 1),
	(464, 844, 21, 3),
	(464, 844, 22, 3),
	(464, 844, 23, 1),
	(464, 844, 24, 4),
	(464, 845, 1, 1),
	(464, 845, 2, 1),
	(464, 845, 8, 1),
	(464, 845, 10, 3),
	(464, 845, 12, 3),
	(464, 845, 13, 4),
	(464, 845, 18, 1),
	(464, 845, 19, 1),
	(464, 845, 20, 1),
	(464, 845, 21, 3),
	(464, 845, 22, 3),
	(464, 845, 23, 1),
	(464, 845, 24, 4),
	(465, 846, 1, 1),
	(465, 846, 2, 1),
	(465, 846, 8, 1),
	(465, 846, 11, 3),
	(465, 846, 12, 3),
	(465, 846, 15, 3),
	(465, 846, 16, 3),
	(465, 846, 20, 1),
	(465, 846, 23, 1),
	(465, 846, 27, 1),
	(465, 846, 28, 3),
	(465, 846, 31, 1),
	(465, 846, 32, 1),
	(465, 846, 33, 3),
	(466, 847, 1, 1),
	(466, 847, 2, 1),
	(466, 847, 8, 1),
	(466, 847, 12, 4),
	(466, 847, 20, 1),
	(466, 847, 27, 1),
	(466, 847, 28, 9),
	(466, 847, 31, 1),
	(466, 847, 32, 1),
	(466, 847, 33, 4),
	(466, 848, 1, 1),
	(466, 848, 2, 1),
	(466, 848, 8, 1),
	(466, 848, 10, 2),
	(466, 848, 12, 2),
	(466, 848, 13, 2),
	(466, 848, 18, 1),
	(466, 848, 19, 1),
	(466, 848, 20, 1),
	(466, 848, 21, 2),
	(466, 848, 22, 2),
	(466, 848, 23, 1),
	(466, 848, 24, 2),
	(467, 849, 26, 1),
	(472, 862, 25, 1),
	(473, 863, 25, 1),
	(474, 864, 25, 1);
/*!40000 ALTER TABLE `order_content` ENABLE KEYS */;

-- Dumping structure for table project_crisis.person
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `weight` int(11) DEFAULT 0,
  `birth_year` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_person_order` (`order_id`),
  KEY `FK_person_person_category` (`category_id`),
  CONSTRAINT `FK_person_order` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
  CONSTRAINT `FK_person_person_category` FOREIGN KEY (`category_id`) REFERENCES `person_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=865 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.person: ~74 rows (approximately)
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` (`id`, `order_id`, `category_id`, `weight`, `birth_year`) VALUES
	(5, 2, 1, 0, 1975),
	(6, 2, 1, 0, 1978),
	(7, 2, 2, 10, 0),
	(62, 20, 1, 0, 1975),
	(63, 20, 1, 0, 1996),
	(64, 20, 1, 0, 2014),
	(74, 24, 1, 0, 1975),
	(75, 24, 1, 0, 1996),
	(76, 24, 1, 0, 2014),
	(94, 39, 1, 0, 1975),
	(95, 39, 1, 0, 1996),
	(96, 39, 1, 0, 2014),
	(799, 429, 1, 0, 1980),
	(800, 429, 1, 0, 1987),
	(841, 463, 1, 0, 1986),
	(842, 463, 1, 0, 2000),
	(843, 463, 2, 11, 0),
	(844, 464, 1, 0, 1972),
	(845, 464, 1, 0, 1980),
	(846, 465, 1, 0, 1987),
	(847, 466, 1, 0, 1987),
	(848, 466, 1, 0, 2015),
	(849, 467, 2, 12, 0),
	(862, 472, 4, 12, 0),
	(863, 473, 4, 80, 0),
	(864, 474, 4, 15, 0);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;

-- Dumping structure for table project_crisis.person_category
CREATE TABLE IF NOT EXISTS `person_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table project_crisis.person_category: ~3 rows (approximately)
/*!40000 ALTER TABLE `person_category` DISABLE KEYS */;
INSERT INTO `person_category` (`id`, `category`) VALUES
	(1, 'human'),
	(2, 'cat'),
	(4, 'dog');
/*!40000 ALTER TABLE `person_category` ENABLE KEYS */;

-- Dumping structure for table project_crisis.person_condition
CREATE TABLE IF NOT EXISTS `person_condition` (
  `person_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  PRIMARY KEY (`person_id`,`condition_id`),
  KEY `FK_person_condition_condition` (`condition_id`),
  CONSTRAINT `FK_person_condition_condition` FOREIGN KEY (`condition_id`) REFERENCES `condition` (`id`),
  CONSTRAINT `FK_person_condition_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.person_condition: ~121 rows (approximately)
/*!40000 ALTER TABLE `person_condition` DISABLE KEYS */;
INSERT INTO `person_condition` (`person_id`, `condition_id`) VALUES
	(5, 3),
	(6, 6),
	(6, 10),
	(7, 8),
	(62, 4),
	(62, 5),
	(63, 2),
	(63, 3),
	(64, 10),
	(74, 4),
	(74, 5),
	(75, 2),
	(75, 3),
	(76, 10),
	(94, 4),
	(94, 5),
	(95, 2),
	(95, 3),
	(96, 10),
	(799, 6),
	(799, 10),
	(800, 3),
	(841, 6),
	(841, 10),
	(842, 3),
	(843, 8),
	(844, 10),
	(845, 10),
	(846, 5),
	(846, 10),
	(847, 5),
	(847, 6),
	(847, 10),
	(848, 10),
	(849, 8),
	(862, 9),
	(863, 9),
	(864, 9);
/*!40000 ALTER TABLE `person_condition` ENABLE KEYS */;

-- Dumping structure for table project_crisis.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `product_category_id` int(11) NOT NULL DEFAULT 0,
  `calories` int(11) NOT NULL DEFAULT 0,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `weight` int(11) NOT NULL DEFAULT 0,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `picture` longtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_product_product_category` (`product_category_id`),
  CONSTRAINT `FK_product_product_category` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.product: ~4 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `product_name`, `product_category_id`, `calories`, `price`, `weight`, `description`, `picture`) VALUES
	(1, 'riis', 1, 3460, 1.59, 1000, 'pikateraline riis - söö palju jaksad', 'https://m1.selver.ee/media/catalog/product/cache/1/image/409x/9df78eab33525d08d6e5fb8d27136e95/4/7/4750020030292.jpg'),
	(2, 'pasta tavaline', 1, 1805, 1.49, 500, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4750020051631.jpg'),
	(7, 'pasta gluteenivaba', 1, 1424, 1.99, 400, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/8/0/8008857114484.jpg'),
	(8, 'kaerahelbed', 1, 1825, 0.39, 500, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4750020030902.jpg'),
	(9, 'kaerahelbed gluteenivabad', 1, 1850, 1.99, 500, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/6/4/6411200107163.jpg'),
	(10, 'konserv metssea', 2, 537, 3.19, 240, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740618000691.jpg'),
	(11, 'konserv lihaga laktoosivaba', 2, 477, 3.39, 240, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740618000684.jpg'),
	(12, 'konserv ubadega', 2, 382, 1.29, 425, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/3/0/3083680001793.jpg'),
	(13, 'bolognese kaste', 4, 367, 1.99, 490, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740670000028.jpg'),
	(14, 'bolognese kaste (gluteenivaba)', 4, 367, 1.99, 490, '', 'https://m1.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/7/3/7311310034047.jpg'),
	(15, 'uncle bens kaste (laktoosivaba)', 4, 344, 1.49, 400, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/7/3/7311310028763.jpg'),
	(16, 'Kookose-karri kaste (gluteeni- ja laktoosivaba)', 4, 521, 3.99, 350, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4744481010085.jpg'),
	(18, 'Piimašokolaad pähklitega', 3, 1100, 2.99, 200, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/6/4/6411401015151.jpg'),
	(19, 'Piimašokolaad', 3, 549, 1.39, 100, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740012034728.jpg'),
	(20, 'Tume šokolaad mandlitega (laktoosivaba)', 3, 1674, 4.19, 300, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740012037019.jpg'),
	(21, 'Sprotid õlis', 2, 381, 1.49, 100, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740481000552.jpg'),
	(22, 'Hautatud veiselihaga konserv', 2, 345, 1.29, 200, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740215240025.jpg'),
	(23, 'Premium Pähklite segu', 3, 1550, 4.29, 250, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4742155002015.jpg'),
	(24, 'Bechamel valge koorekaste', 4, 346, 2.89, 350, '', 'https://m1.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/4/7/4744481010191.jpg'),
	(25, 'Koera kuivtoit liha-juurvili', 5, 0, 16.99, 10000, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/7/6/7613032933807.jpg'),
	(26, 'Kuiv kassitoit kana/riis', 6, 0, 4.79, 800, '', 'https://www.selver.ee/media/catalog/product/cache/1/image/800x/9df78eab33525d08d6e5fb8d27136e95/8/0/8002205358902.jpg'),
	(27, 'Tatrakakud šokolaadiga ÖKO', 3, 332, 2.39, 70, '', 'https://m1.selver.ee/media/catalog/product/cache/1/image/409x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740436000392.jpg'),
	(28, 'Pomi tomatikaste (sobilik kõigile)', 4, 310, 1.79, 500, '', 'https://m1.selver.ee/media/catalog/product/cache/1/image/409x/9df78eab33525d08d6e5fb8d27136e95/8/0/8002580022702.jpg'),
	(30, 'Vegan majonees', 4, 1765, 3.19, 270, '0', 'https://www.selver.ee/media/catalog/product/cache/1/image/409x/9df78eab33525d08d6e5fb8d27136e95/8/7/8717163780527.jpg'),
	(31, 'Kookoshelbeküpsised (sobilik kõigile)', 3, 448, 3.19, 100, '0', 'https://m1.selver.ee/media/catalog/product/cache/1/image/409x/9df78eab33525d08d6e5fb8d27136e95/4/7/4740436001276.jpg'),
	(32, 'Tatar (sobilik kõigile)', 1, 1000, 1.59, 3440, '0', 'https://m1.selver.ee/media/catalog/product/cache/1/image/409x/9df78eab33525d08d6e5fb8d27136e95/4/7/4750020030919_2.jpg'),
	(33, 'Konservherned (sobilik kõigile)', 2, 496, 1.39, 680, '0', 'https://m1.selver.ee/media/catalog/product/cache/1/image/409x/9df78eab33525d08d6e5fb8d27136e95/5/4/5410153272981.jpg');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table project_crisis.product_category
CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.product_category: ~6 rows (approximately)
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` (`id`, `category`) VALUES
	(1, 'grains'),
	(2, 'canned food'),
	(3, 'sweets'),
	(4, 'sauces'),
	(5, 'dog food'),
	(6, 'cat food');
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;

-- Dumping structure for table project_crisis.product_condition
CREATE TABLE IF NOT EXISTS `product_condition` (
  `product_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`condition_id`),
  KEY `FK_product_condition_condition` (`condition_id`),
  CONSTRAINT `FK_product_condition_condition` FOREIGN KEY (`condition_id`) REFERENCES `condition` (`id`),
  CONSTRAINT `FK_product_condition_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.product_condition: ~100 rows (approximately)
/*!40000 ALTER TABLE `product_condition` DISABLE KEYS */;
INSERT INTO `product_condition` (`product_id`, `condition_id`) VALUES
	(1, 2),
	(1, 3),
	(1, 4),
	(1, 5),
	(1, 6),
	(1, 10),
	(2, 2),
	(2, 3),
	(2, 5),
	(2, 6),
	(2, 10),
	(7, 2),
	(7, 3),
	(7, 4),
	(7, 10),
	(7, 11),
	(8, 2),
	(8, 3),
	(8, 5),
	(8, 6),
	(8, 10),
	(9, 2),
	(9, 3),
	(9, 4),
	(9, 10),
	(9, 11),
	(10, 4),
	(10, 6),
	(10, 10),
	(11, 4),
	(11, 5),
	(11, 10),
	(11, 11),
	(12, 2),
	(12, 3),
	(12, 4),
	(12, 5),
	(12, 6),
	(12, 10),
	(13, 2),
	(13, 6),
	(13, 10),
	(14, 2),
	(14, 4),
	(14, 10),
	(14, 11),
	(15, 2),
	(15, 3),
	(15, 5),
	(15, 10),
	(15, 11),
	(16, 2),
	(16, 3),
	(16, 4),
	(16, 5),
	(16, 10),
	(16, 11),
	(18, 2),
	(18, 4),
	(18, 10),
	(19, 2),
	(19, 4),
	(19, 6),
	(19, 10),
	(20, 2),
	(20, 3),
	(20, 4),
	(20, 5),
	(20, 6),
	(20, 10),
	(21, 4),
	(21, 6),
	(21, 10),
	(22, 4),
	(22, 6),
	(22, 10),
	(23, 2),
	(23, 3),
	(23, 4),
	(23, 5),
	(23, 10),
	(24, 2),
	(24, 4),
	(24, 6),
	(24, 10),
	(25, 9),
	(26, 8),
	(27, 2),
	(27, 3),
	(27, 4),
	(27, 5),
	(27, 6),
	(27, 10),
	(27, 11),
	(28, 2),
	(28, 3),
	(28, 4),
	(28, 5),
	(28, 6),
	(28, 10),
	(28, 11),
	(30, 3),
	(31, 2),
	(31, 3),
	(31, 4),
	(31, 5),
	(31, 6),
	(31, 10),
	(31, 11),
	(32, 2),
	(32, 3),
	(32, 4),
	(32, 5),
	(32, 6),
	(32, 10),
	(32, 11),
	(33, 2),
	(33, 3),
	(33, 4),
	(33, 5),
	(33, 6),
	(33, 10),
	(33, 11);
/*!40000 ALTER TABLE `product_condition` ENABLE KEYS */;

-- Dumping structure for table project_crisis.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table project_crisis.user: ~1 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`) VALUES
	(1, 'admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
