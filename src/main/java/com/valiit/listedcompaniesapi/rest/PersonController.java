package com.valiit.listedcompaniesapi.rest;



import com.valiit.listedcompaniesapi.dto.PersonDto;
import com.valiit.listedcompaniesapi.model.Order;
import com.valiit.listedcompaniesapi.model.Person;
import com.valiit.listedcompaniesapi.model.Product;
import com.valiit.listedcompaniesapi.repository.PersonRepository;
import com.valiit.listedcompaniesapi.repository.ProductRepository;
import com.valiit.listedcompaniesapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
@CrossOrigin("*")
public class PersonController {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ProductService productService;


    @GetMapping
    public List<Person> getPerson() {
        return personRepository.getPerson();
    }



}