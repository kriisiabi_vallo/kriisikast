package com.valiit.listedcompaniesapi.rest;


import com.valiit.listedcompaniesapi.dto.PersonDto;
import com.valiit.listedcompaniesapi.model.Person;
import com.valiit.listedcompaniesapi.model.Product;
import com.valiit.listedcompaniesapi.repository.PersonRepository;
import com.valiit.listedcompaniesapi.repository.ProductRepository;
import com.valiit.listedcompaniesapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personDto")
@CrossOrigin("*")
public class PersonDtoController {
    @Autowired
    private ProductService productService;
    @Autowired
    private PersonController personController;
    @Autowired
    private ProductController productController;


    @GetMapping
    public List<PersonDto> getPersonDto() {
        return productService.personDtoList(personController.getPerson(), productController.getProduct());
    }

}
