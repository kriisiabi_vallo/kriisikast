package com.valiit.listedcompaniesapi.rest;


import com.valiit.listedcompaniesapi.dto.OrderDto;
import com.valiit.listedcompaniesapi.dto.PersonDto;
import com.valiit.listedcompaniesapi.model.Order;
import com.valiit.listedcompaniesapi.model.Person;
import com.valiit.listedcompaniesapi.repository.OrderRepository;
import com.valiit.listedcompaniesapi.repository.PersonRepository;
import com.valiit.listedcompaniesapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
@CrossOrigin("*")

public class OrderController {


    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ProductService productService;

    @PostMapping
    public OrderDto addOrderInfo(@RequestBody Order order) {
        int orderId = orderRepository.addPersons(order);
        return orderRepository.getOrderForTable(orderId).get(0);
    }

    @GetMapping("/{id}")
    public OrderDto getOrder(@PathVariable("id") int id) {
        return orderRepository.getOrderForTable(id).get(0);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable("id") int id) {
        orderRepository.deleteOrder(id);
    }

    @GetMapping
    public List<OrderDto> getAll() { return orderRepository.getAllOrders(orderRepository.getAllOrderNumbers());}
}


