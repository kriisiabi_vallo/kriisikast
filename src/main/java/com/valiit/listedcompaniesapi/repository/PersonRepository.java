package com.valiit.listedcompaniesapi.repository;


import com.valiit.listedcompaniesapi.model.Order;
import com.valiit.listedcompaniesapi.model.Person;
import com.valiit.listedcompaniesapi.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class PersonRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Integer> getConditions(int personId) {
        return jdbcTemplate.query("select* from person_condition where person_id=?", new Object[]{personId}, (row, number)-> row.getInt("condition_id"));
    }

    public List<String> getConditionsWords(int personId) {
        return jdbcTemplate.query("SELECT c.condition from person_condition inner JOIN `condition` c on person_condition.condition_id=c.id where person_id=?", new Object[]{personId}, (row, number)->{
            return row.getString("c.condition");
        });
    }

    public List<Person> getPerson() {
        return jdbcTemplate.query("select * from person", mapPersonRows);
    }

    private RowMapper<Person> mapPersonRows = (rs, rowNum) -> {
        Person person = new Person();
        person.setId(rs.getInt("id"));
        person.setOrder_id(rs.getInt("order_id"));
        person.setCategory(rs.getInt("category_id"));
        person.setWeight(rs.getInt("weight"));
        person.setBirthYear(rs.getInt("birth_year"));
        person.setConditions(getConditions(rs.getInt("id")));
        person.setConditionsWords(getConditionsWords(rs.getInt("id")));

        return person;
    };





}
