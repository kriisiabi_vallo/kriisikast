package com.valiit.listedcompaniesapi.repository;

import com.valiit.listedcompaniesapi.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Integer> getConditionIds(int productId) {
        return jdbcTemplate.query("select* from product_condition where product_id=?", new Object[]{productId}, (row, number)->{
            return row.getInt("condition_id");
        });
    }

    public List<Product> getProduct() {
        return jdbcTemplate.query("select * from product", mapProductRows);
    }

    private RowMapper<Product> mapProductRows = (rs, rowNum) -> {
        Product product = new Product();
        product.setId(rs.getInt("id"));
        product.setProductName(rs.getString("product_name"));
        product.setProductCategoryId(rs.getInt("product_category_id"));
        product.setCalories(rs.getInt("calories"));
        product.setPrice(rs.getDouble("price"));
        product.setWeight(rs.getInt("weight"));
        product.setDescription(rs.getString("description"));
        product.setImage(rs.getString("picture"));
        product.setCondition(getConditionIds(rs.getInt("id")));
        return product;
    };



}
