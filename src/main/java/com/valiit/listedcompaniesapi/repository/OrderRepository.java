package com.valiit.listedcompaniesapi.repository;


import com.valiit.listedcompaniesapi.dto.OrderDto;
import com.valiit.listedcompaniesapi.dto.PersonDto;
import com.valiit.listedcompaniesapi.dto.ProductDto;
import com.valiit.listedcompaniesapi.model.Order;
import com.valiit.listedcompaniesapi.model.Person;
import com.valiit.listedcompaniesapi.model.Product;
import com.valiit.listedcompaniesapi.rest.OrderController;
import com.valiit.listedcompaniesapi.rest.PersonController;
import com.valiit.listedcompaniesapi.rest.ProductController;
import com.valiit.listedcompaniesapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class OrderRepository {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductController productController;
    @Autowired
    private PersonController personController;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int addNewOrderToOrderTable(Order newOrder) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = formatter.format(new Date());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(
                                    "INSERT INTO `order` (date, email, delivery_method, name, delivery_address, quantity) VALUES (?, ?, ?, ?, ?, ?)",
                                    Statement.RETURN_GENERATED_KEYS
                            );
                    ps.setString(1, formattedDate);
                    ps.setString(2, newOrder.getEmail());
                    ps.setInt(3, newOrder.getDeliveryMethod());
                    ps.setString(4, newOrder.getName());
                    ps.setString(5, newOrder.getDeliveryAddress());
                    ps.setInt(6, newOrder.getOrderQuantity());
                    return ps;
                },
                keyHolder);
        return keyHolder.getKey().intValue();

    }



    public void addOrderContent(List<Person> personList) {

        List<PersonDto> personDtoList = productService.personDtoList(personList, productController.getProduct());
        for (int j = 0; j < personDtoList.size(); j++) {

            for (int i = 0; i < personDtoList.get(j).getFinalProducts().size(); i++) {
                jdbcTemplate.update(
                        "insert into order_content (order_id, person_id, product_id, quantity) values (?, ?, ?, ?)",
                        personDtoList.get(j).getOrder_id(), personDtoList.get(j).getId(), personDtoList.get(j).getFinalProducts().get(i).getId(), personDtoList.get(j).getFinalProducts().get(i).getQuantity()
                );
            }
        }
    }

    public int addPersons(Order order) {
        int orderId = addNewOrderToOrderTable(order);

        for(int i = 0; i<order.getPersonList().size(); i++) {
            order.getPersonList().get(i).setOrder_id(orderId);
        }

        for(int i =0; i<order.getPersonList().size(); i++) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            int orderIdInsert =order.getPersonList().get(i).getOrder_id();
            int categoryIdInsert = order.getPersonList().get(i).getCategory();
            int weightInsert = order.getPersonList().get(i).getWeight();
            int birthYearInsert = order.getPersonList().get(i).getBirthYear();
            jdbcTemplate.update(connection -> {
                        PreparedStatement ps =
                                connection.prepareStatement(
                                        "INSERT INTO person (order_id, category_id, weight, birth_year) VALUES (?, ?, ?, ?)",
                                        Statement.RETURN_GENERATED_KEYS
                                );
                        ps.setInt(1, orderIdInsert );
                        ps.setInt(2, categoryIdInsert);
                        ps.setInt(3, weightInsert);
                        ps.setInt(4, birthYearInsert);
                        return ps;
                    },
                    keyHolder);
            order.getPersonList().get(i).setId(keyHolder.getKey().intValue());



            for(int j = 0; j< order.getPersonList().get(i).getConditions().size(); j++) {
                jdbcTemplate.update(
                        "insert into person_condition (person_id, condition_id) values (?, ?)",
                        order.getPersonList().get(i).getId(), order.getPersonList().get(i).getConditions().get(j)

                );
            }

        }

        addOrderContent(order.getPersonList());

        return orderId;


    }
    public List<Integer> getAllOrderNumbers() {
        return jdbcTemplate.query("select*from `order`", (row, number)->{
            return row.getInt("id");
        });

    }

    public List<OrderDto> getAllOrders(List<Integer> idList) {
        List<OrderDto> allOrders = new ArrayList<>();
        for(int i=0; i<idList.size(); i++) {
            allOrders.add(getOrderForTable(idList.get(i)).get(0));
        }

        return allOrders;
    }

    public List<OrderDto> getOrderForTable(int orderId) {
        return jdbcTemplate.query("select * from `order` where id = ?",new Object[]{orderId}, mapOrderRows);
    }

    private RowMapper<OrderDto> mapOrderRows = (rs, rowNum) -> {
        OrderDto orderDto = new OrderDto();
        orderDto.setEmail(rs.getString("email"));
        orderDto.setDeliveryMethod(rs.getInt("delivery_method"));
        orderDto.setName(rs.getString("name"));
        orderDto.setDeliveryAddress(rs.getString("delivery_address"));
        orderDto.setOrderQuantity(rs.getInt("quantity"));
        orderDto.setPersonDtoList(getPersonsOnOrder(rs.getInt("id")));

        return orderDto;
    };


    public List<PersonDto> getPersonsOnOrder(int orderId) {
        return jdbcTemplate.query("select * from person where order_id = ?",new Object[]{orderId}, mapPersonRows);
    }
    private RowMapper<PersonDto> mapPersonRows = (rs, rowNum) -> {
        PersonDto personDto = new PersonDto();
        personDto.setId(rs.getInt("id"));
        personDto.setOrder_id(rs.getInt("order_id"));
        personDto.setCategory(rs.getInt("category_id"));
        personDto.setWeight(rs.getInt("weight"));
        personDto.setBirthYear(rs.getInt("birth_year"));
        personDto.setConditions(getPersonCondition(rs.getInt("id")));
        personDto.setConditionsWords(getPersonConditionsWords(rs.getInt("id")));
        personDto.setFinalProducts(getProductDtoFinalList(rs.getInt("id")));

        return personDto;
    };

    public List<Integer> getPersonCondition(int personId) {
        return jdbcTemplate.query("select* from person_condition where person_id=?", new Object[]{personId}, (row, number)->{
            return row.getInt("condition_id");
        });
    }

    public List<String> getPersonConditionsWords(int personId) {
        return jdbcTemplate.query("SELECT c.condition from person_condition inner JOIN `condition` c on person_condition.condition_id=c.id where person_id=?", new Object[]{personId}, (row, number)->{
            return row.getString("c.condition");
        });
    }

    public List<ProductDto> getProductDtoFinalList (int personId) {
        return jdbcTemplate.query("SELECT oc.product_id, oc.quantity, product.product_name,  product.product_category_id, product_category.category, product.calories, product.price, product.picture, product.weight, product.description, p.id FROM order_content oc INNER JOIN person p ON oc.person_id=p.id INNER JOIN product ON oc.product_id = product.id INNER JOIN product_category ON product.product_category_id=product_category.id where oc.person_id = ?",new Object[]{personId}, mapProductRows);
    }

    private RowMapper<ProductDto> mapProductRows = (rs, rowNum) -> {
        ProductDto productDto = new ProductDto();
        productDto.setId(rs.getInt("oc.product_id"));
        productDto.setProductName(rs.getString("product.product_name"));
        productDto.setProductCategoryId(rs.getInt("product.product_category_id"));
        productDto.setCalories(rs.getInt("product.calories"));
        productDto.setPrice(rs.getDouble("product.price"));
        productDto.setWeight(rs.getInt("product.weight"));
        productDto.setDescription(rs.getString("product.description"));
        productDto.setImage(rs.getString("product.picture"));
        productDto.setQuantity(rs.getInt("oc.quantity"));

        return productDto;
    };

    public void deleteOrder(int id) {
        jdbcTemplate.update("DELETE person_condition FROM person_condition INNER JOIN person ON person_condition.person_id=person.id WHERE person.order_id=?", id);
        jdbcTemplate.update("DELETE order_content FROM order_content WHERE order_id=?", id);
        jdbcTemplate.update("DELETE person FROM person WHERE order_id=?", id);
        jdbcTemplate.update("DELETE `order` FROM `order` WHERE id=?", id);

    }

}
