//package com.valiit.listedcompaniesapi.util;
//
//import com.valiit.listedcompaniesapi.dto.CompanyDto;
//import com.valiit.listedcompaniesapi.model.Company;
//
//public class Transformer {
//
//    public static Company toCompanyModel(CompanyDto initialObject) {
//        if (initialObject == null) {
//            return null;
//        }
//
//        Company resultingObject = new Company();
//        resultingObject.setId(initialObject.getId());
//        resultingObject.setName(initialObject.getName());
//        resultingObject.setLogo(initialObject.getLogo());
//        resultingObject.setEstablished(initialObject.getEstablished());
//        resultingObject.setEmployees(initialObject.getEmployees());
//        resultingObject.setRevenue(initialObject.getRevenue());
//        resultingObject.setNetIncome(initialObject.getNetIncome());
//        resultingObject.setSecurities(initialObject.getSecurities());
//        resultingObject.setSecurityPrice(initialObject.getSecurityPrice());
//        resultingObject.setDividends(initialObject.getDividends());
//        return resultingObject;
//    }
//
//    public static CompanyDto toCompanyDto(Company initialObject) {
//        if (initialObject == null) {
//            return null;
//        }
//
//        CompanyDto resultingObject = new CompanyDto();
//        resultingObject.setId(initialObject.getId());
//        resultingObject.setName(initialObject.getName());
//        resultingObject.setLogo(initialObject.getLogo());
//        resultingObject.setEstablished(initialObject.getEstablished());
//        resultingObject.setEmployees(initialObject.getEmployees());
//        resultingObject.setRevenue(initialObject.getRevenue());
//        resultingObject.setNetIncome(initialObject.getNetIncome());
//        resultingObject.setSecurities(initialObject.getSecurities());
//        resultingObject.setSecurityPrice(initialObject.getSecurityPrice());
//        resultingObject.setDividends(initialObject.getDividends());
//
//        if (initialObject.getSecurities() > 0 && initialObject.getSecurityPrice() > 0) {
//            Double marketCapitalization = Helper.round(initialObject.getSecurities() * initialObject.getSecurityPrice());
//            resultingObject.setMarketCapitalization(marketCapitalization);
//        }
//
//        if (initialObject.getSecurityPrice() > 0 && initialObject.getDividends() > 0) {
//            Double dividendYield = Helper.round(initialObject.getDividends() / initialObject.getSecurityPrice());
//            resultingObject.setDividendYield(dividendYield);
//        }
//
//        return resultingObject;
//    }
//}
