package com.valiit.listedcompaniesapi.service;

import com.valiit.listedcompaniesapi.dto.PersonDto;
import com.valiit.listedcompaniesapi.dto.ProductDto;
import com.valiit.listedcompaniesapi.model.Person;
import com.valiit.listedcompaniesapi.model.Product;
import com.valiit.listedcompaniesapi.repository.PersonRepository;
import com.valiit.listedcompaniesapi.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private ProductRepository productRepository;
    private PersonRepository personRepository;

    public List<PersonDto> personDtoList(List<Person> person, List<Product> productList) {
        List<PersonDto> personDtoList = new ArrayList<>();
        for (int i = 0; i < person.size(); i++) {
            personDtoList.add(finalProductsForPerson(getProductsForPerson(person.get(i), productList), productList));

        }

        return personDtoList;
    }


    public PersonDto getProductsForPerson(Person person, List<Product> productList) {
        PersonDto personDto = new PersonDto();
        personDto.setId(person.getId());
        personDto.setOrder_id(person.getOrder_id());
        personDto.setCategory(person.getCategory());
        personDto.setWeight(person.getWeight());
        personDto.setBirthYear(person.getBirthYear());
        personDto.setConditions(person.getConditions());


        List<Product> allValidProducts = getApplicableProducts(person, productList);
        personDto.setProducts(allValidProducts.stream().map(p -> p.getId()).collect(Collectors.toList()));


        return personDto;
    }

    private List<Product> getApplicableProducts(Person person, List<Product> productList) {
        List<Product> applicableProducts;
        List<Product> allergyProducts = new ArrayList<>();
        List<Product> nonAllergyProducts = new ArrayList<>();

        if(person.getConditions().size()==1) {
        for(int i = 0; i< productList.size(); i++) {
            if(!(isAllergyProduct(productList.get(i)))){
                nonAllergyProducts.add(productList.get(i));
            }
        } applicableProducts=nonAllergyProducts;}


//        if (person.getConditions().size() == 1) {
//            for (int i = 0; i < productList.size(); i++) {
//                for (int j = 0; j < productList.get(i).getCondition().size(); j++) {
//                    if (productList.get(i).getCondition().get(j) == 11) {
//                        allergyProducts.add(productList.get(i));
//                    }
//                }
//            }
//            int i = 0;
//            int j = 0;
//           do {
//              if(nonAllergyProducts.get(j).getId()==allergyProducts.get(i).getId()) {
//                  nonAllergyProducts.remove(j);
//                  i++;
//              } else {j++; i++;}
//
//
//           } while (allergyProducts.get(i).getId()==allergyProducts.get(allergyProducts.size()-1).getId());
//            applicableProducts = nonAllergyProducts;
//
         else {
            applicableProducts = productList;
       }


        List<Integer> conditions = new ArrayList<>();


        for (int i = 0; i < person.getConditions().size(); i++) {
            applicableProducts = this.filterProducts(person.getConditions().get(i), applicableProducts);
        }
        return applicableProducts;
    }

    private boolean isAllergyProduct (Product product) {
        for(int i=0; i <product.getCondition().size(); i++) {
            if(product.getCondition().get(i)==11) {
                return true;
            }
            } return false;
        }



    private List<Product> filterProducts(int conditionId, List<Product> productList) {
        List<Product> validProducts = new ArrayList<>();

        for (int i = 0; i < productList.size(); i++) {
            for (int j = 0; j < productList.get(i).getCondition().size(); j++) {
                if (conditionId == productList.get(i).getCondition().get(j)) {
                    validProducts.add(productList.get(i));
                }
            }
        }

        return validProducts;
    }


    public int calorieCalculator(PersonDto personDto) {
        int year = Year.now().getValue();
        if (personDto.getCategory() == 1) {
            if ((year - personDto.getBirthYear()) > 18) {
                return 1900 * 7;
            } else {
                return childCalorieIntake((year - personDto.getBirthYear())) * 7;
            }
        } else if (personDto.getCategory() == 2) {
            return catGramIntake(personDto.getWeight()) * 7;
        } else {
            return dogGramIntake(personDto.getWeight()) * 7;
        }

    }

    public int childCalorieIntake(int year) {
        if (year > 14) {
            return 2200;
        } else if (year > 9) {
            return 1800;
        } else if (year > 4) {
            return 1400;
        } else if (year > 2) {
            return 1000;
        } else {
            return 900;
        }
    }

    public int catGramIntake(int weight) {
        if (weight > 10) {
            return 100;
        } else if (weight > 8) {
            return 80;
        } else if (weight > 6) {
            return 70;
        } else if (weight > 4) {
            return 55;
        } else {
            return 45;
        }
    }

    public int dogGramIntake(int weight) {
        if (weight > 50) {
            return 800;
        } else if (weight > 41) {
            return 550;
        } else if (weight > 31) {
            return 450;
        } else if (weight > 26) {
            return 380;
        } else if (weight > 16) {
            return 330;
        } else if (weight > 10) {
            return 230;
        } else {
            return 150;
        }
    }

    public PersonDto addingProductDtoToPersonDto(PersonDto personDto, List<Product> productList) {
        Map<Integer, Product> productMap = new HashMap<>();
        int quantity = 0;

        for (int i = 0; i < productList.size(); i++) {
            productMap.put(productList.get(i).getId(), productList.get(i));
        }


        List<ProductDto> productDtoList = new ArrayList<>();
        for (int i = 0; i < personDto.getProducts().size(); i++) {
            ProductDto productDto = new ProductDto();
            productDto.setId(personDto.getProducts().get(i));
            productDto.setProductName(productMap.get(personDto.getProducts().get(i)).getProductName());
            productDto.setProductCategoryId(productMap.get(personDto.getProducts().get(i)).getProductCategoryId());
            productDto.setCalories(productMap.get(personDto.getProducts().get(i)).getCalories());
            productDto.setPrice(productMap.get(personDto.getProducts().get(i)).getPrice());
            productDto.setWeight(productMap.get(personDto.getProducts().get(i)).getWeight());
            productDto.setDescription(productMap.get(personDto.getProducts().get(i)).getDescription());
            productDto.setImage(productMap.get(personDto.getProducts().get(i)).getImage());
            productDto.setCondition(productMap.get(personDto.getProducts().get(i)).getCondition());

            productDtoList.add(productDto);

        }
        personDto.setFinalProducts(productDtoList);
        return personDto;
    }

    public PersonDto finalProductsForPerson(PersonDto personDto, List<Product> productList) {

        personDto = addingProductDtoToPersonDto(personDto, productList);


        if (personDto.getCategory() == 2) {
            int grams = calorieCalculator(personDto);
            int foodGrams = 0;
            int quantity = 0;
            do {
                foodGrams = foodGrams + personDto.getFinalProducts().get(0).getWeight();
                quantity++;

            } while (foodGrams <= grams);

            personDto.getFinalProducts().get(0).setQuantity(quantity);
            return personDto;

        } else if (personDto.getCategory() == 4) {
            int grams = calorieCalculator(personDto);
            int foodGrams = 0;
            int quantity = 0;
            do {
                foodGrams = foodGrams + personDto.getFinalProducts().get(0).getWeight();
                quantity++;

            } while (foodGrams < grams);

            personDto.getFinalProducts().get(0).setQuantity(quantity);
            return personDto;
        } else {
            int calories = calorieCalculator(personDto);
            double grainsNeeded = calories * 0.5;
            double cannedFoodNeeded = calories * 0.25;
            double saucesNeeded = calories * 0.2;
            double sweetsNeeded = calories * 0.05;

            int grains = 0;
            int cannedFood = 0;
            int sauce = 0;
            int sweets = 0;

            do {
                for (int i = 0; i < personDto.getFinalProducts().size(); i++) {
                    if (personDto.getFinalProducts().get(i).getProductCategoryId() == 1) {

                        grains = grains + personDto.getFinalProducts().get(i).getCalories();
                        personDto.getFinalProducts().get(i).setQuantity(personDto.getFinalProducts().get(i).getQuantity() + 1);

                    }
                }
            }
            while (grains < grainsNeeded);


            do {
                for (int i = 0; i < personDto.getFinalProducts().size(); i++) {
                    if (personDto.getFinalProducts().get(i).getProductCategoryId() == 2) {

                        cannedFood = cannedFood + personDto.getFinalProducts().get(i).getCalories();
                        personDto.getFinalProducts().get(i).setQuantity(personDto.getFinalProducts().get(i).getQuantity() + 1);


                    }
                }
            }
            while (cannedFood < cannedFoodNeeded);

            do {
                for (int i = 0; i < personDto.getFinalProducts().size(); i++) {

                    if (personDto.getFinalProducts().get(i).getProductCategoryId() == 3) {

                        sweets = sweets + personDto.getFinalProducts().get(i).getCalories();
                        personDto.getFinalProducts().get(i).setQuantity(personDto.getFinalProducts().get(i).getQuantity() + 1);

                    }
                }
            }
            while (sweets < sweetsNeeded);


            do {
                for (int i = 0; i < personDto.getFinalProducts().size(); i++) {

                    if (personDto.getFinalProducts().get(i).getProductCategoryId() == 4) {


                        sauce = sauce + personDto.getFinalProducts().get(i).getCalories();
                        personDto.getFinalProducts().get(i).setQuantity(personDto.getFinalProducts().get(i).getQuantity() + 1);

                    }
                }
            }
            while (sauce < saucesNeeded);
            return personDto;
        }

    }
}






