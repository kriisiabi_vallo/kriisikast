package com.valiit.listedcompaniesapi.dto;

import com.valiit.listedcompaniesapi.model.Person;

import java.sql.Date;
import java.util.List;

public class OrderDto {
    private String email;
    private int deliveryMethod;
    private String name;
    private String deliveryAddress;
    private int orderQuantity;
    private List<PersonDto> personDtoList;

    public String getEmail() {
        return email;
    }

    public int getDeliveryMethod() {
        return deliveryMethod;
    }

    public List<PersonDto> getPersonDtoList() {
        return personDtoList;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDeliveryMethod(int deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public void setPersonDtoList(List<PersonDto> personDtoList) {
        this.personDtoList = personDtoList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }


}
