package com.valiit.listedcompaniesapi.model;

import java.util.List;

public class Product {
    private int id;
    private String productName;
    private int productCategoryId;
    private int calories;
    private double price;
    private int weight;
    private String description;
    private String image;
    private List<Integer> condition;

    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public int getCalories() {
        return calories;
    }

    public double getPrice() {
        return price;
    }

    public int getWeight() {
        return weight;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public List<Integer> getCondition() {
        return condition;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setCondition(List<Integer> condition) {
        this.condition = condition;
    }
}
