package com.valiit.listedcompaniesapi.model;

import java.util.List;

public class Order {
    private String email;
    private int deliveryMethod;
    private String name;
    private String deliveryAddress;
    private int orderQuantity;
    private List<Person> personList;

    public String getEmail() {
        return email;
    }

    public int getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDeliveryMethod(int deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }
}
