package com.valiit.listedcompaniesapi.model;

import java.util.List;

public class Person {
    private int id;
    private int order_id;
    private int category;
    private int weight;
    private int birthYear;
    private List<Integer> conditions;
    private List<String> conditionsWords;

    public int getId() {
        return id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public int getCategory() {
        return category;
    }

    public int getWeight() {
        return weight;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public List<Integer> getConditions() {
        return conditions;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public void setConditions(List<Integer> conditions) {
        this.conditions = conditions;
    }

    public List<String> getConditionsWords() {
        return conditionsWords;
    }

    public void setConditionsWords(List<String> conditionsWords) {
        this.conditionsWords = conditionsWords;
    }
}
